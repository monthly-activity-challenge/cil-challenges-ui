export const GET_PARTICIPANTS = "GET_PARTICIPANTS";
export const FETCH_PARTICIPANTS_FAILED = "FETCH_PARTICIPANTS_FAILED";
export const NEW_PARTICIPANT = "NEW_PARTICIPANT";
export const SELECTED_PARTICIPANT = "SELECTED_PARTICIPANT";
export const CLEAR_PARTICIPANT = "CLEAR_PARTICIPANT";
export const ADD_FAILED = "ADD_FAILED";


// let targetURL = 'http://localhost:8080'
let targetURL = 'https://cil-holiday-db.herokuapp.com'

export const getParticipants = () => {
    return dispatch => {
        fetch(`${targetURL}/participant`, 
            {
                method : 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                }
            })
            .then(response => response.json())
            .then(participants => dispatch({
                type: GET_PARTICIPANTS,
                payload: participants
            }))
            .catch(err => dispatch({
                type: FETCH_PARTICIPANTS_FAILED,
                payload: err
            }))
    }
}

export const selectedParticipant = (selectedParticipant) => {
    return dispatch => {
        dispatch
        ({
            type: SELECTED_PARTICIPANT,
            payload: selectedParticipant
        })
    }
}

export const clearParticipant = () => {
    return dispatch => {
        dispatch
        ({
            type: CLEAR_PARTICIPANT,
            payload: ""
        })
    }
}

// export const newParticipant = (newParticipant) => {
//     return dispatch => {
//         dispatch
//         ({
//             type: NEW_PARTICIPANT,
//             payload: newParticipant

//         })
//     }
// }

export const newParticipant = (newParticipant) => {
    return dispatch => {
        fetch(`${targetURL}/participant`,
        {
            method: 'POST',
            body: JSON.stringify(newParticipant),
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }    
        })
        .then(response => response.json())
        .then(response =>
            dispatch({
                type : NEW_PARTICIPANT,
                payload : response
            }))
        .catch(err => dispatch({
            type : ADD_FAILED,
            payload: err
        }))
    }
}
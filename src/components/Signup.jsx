import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Component } from 'react';
import { newParticipant } from '../actions/participantActions'
import { Redirect } from 'react-router';

class Signup extends Component {

    handleParticipantSignup = (e) => {
        e.preventDefault();

        let newParticipant = {
                    leaderboardName : e.target.elements.participant.value.trim(),
                    firstName : e.target.elements.firstName.value,
                    lastName : e.target.elements.lastName.value,
                    classType : e.target.elements.class_focus.value
        }
        console.log(newParticipant);
        this.props.newParticipant(newParticipant);
    }

    render() {
        if (this.props.selectedParticipant){
            return (<Redirect to="/readytoplay"/>)
        }
        return (
            <div className="space-form">
                <h2>Not in the list?  Sign-up here!</h2>
                <form onSubmit={this.handleParticipantSignup}>

                    <label htmlFor="participant">What's your leaderboard name?</label>
                    <input type="text" id="participant" name="participant" required />
                    <br></br>

                    <label htmlFor="firstName">First Name?</label>
                    <input type="text" id="firstName" name="firstName" />
                    <br></br>

                    <label htmlFor="lastName">Last Name?</label>
                    <input type="text" id="lastName" name="lastName" />
                    <br></br>

                    {/* onChange={(event) => this.updateValue(event)} */}
                    <input type="radio" id="bike" name="class_focus" value="bike" />
                    <label htmlFor="bike">I want to ride!</label>
                    <br></br>
                    <input type="radio" id="pzone" name="class_focus" value="power zone" />
                    <label htmlFor="pzone">I like to be in the zone!</label>
                    <br></br>
                    <input type="radio" id="tread" name="class_focus" value="tread" />
                    <label htmlFor="tread">I'd rather run!</label>
                    <br></br>

                    <button className="form-fields">Let's Go</button>
                </form>

                {/* onChange={(event) => this.handleUserInput(event)} > */}

            </div>
        );
    }
}

const mapStateToProps = (state) => (
    {
        selectedParticipant: state.participant.selectedParticipant,
        card: state.challenge
    }
)

const mapDispatchToProps = dispatch => bindActionCreators({
    newParticipant
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Signup)
import React, { Component } from "react";
import { OrnaTest } from "./CSSOrnament/OrnamentTest";

class ChallengeClass extends Component {

    updateValue = (e) => {
        this.props.card.completionDate = e.target.value;
        this.props.captureChange(this.props.card)
    }

    render() {
        return (
            <div>
                <OrnaTest >
                <label className="emphasis-text" htmlFor="class">{this.props.card.classType}<br/>
                {this.props.card.instructorName}<br/></label>
                <input type="date" onChange={(event) => this.updateValue(event)} id="class" name="class"
                    readOnly={(this.props.card.completionDate === null) ? false : true} defaultValue={this.props.card.completionDate} />
                </OrnaTest>
            </div>
        )
    }
}

export default ChallengeClass;
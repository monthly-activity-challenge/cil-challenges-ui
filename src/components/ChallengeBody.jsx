import { Component } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ChallengeClass from "./ChallengeClass";
import { updateCard } from '../actions/challengeActions'
import { selectedParticipant } from "../actions/participantActions";

class ChallengeBody extends Component {

    state = {
        classesSaved : false
    }
    
    updatedClasses = [];
    updatesMade = false;

    saveChanges = (e) => {
        e.preventDefault()
        this.updatedClasses = this.updatedClasses.filter((card, index) => this.updatedClasses.indexOf(card) === index ? card : null);
        //Don't think there is a need to send all the classes back.  Just updated ones?
        // this.updatedClasses.forEach(savedClass => this.props.card.splice(savedClass.id, 1, savedClass));
        this.props.updateCard(this.updatedClasses, this.props.selectedParticipant); 
        this.setState({ classesSaved : true });
        this.updatesMade = this.state.classesSaved;

    }

    holdChanges = (card) => {
        this.updatedClasses.push(card)
    }

    render() {

        return (
            <div>
                <h2 className="white-font">{this.props.selectedParticipant}</h2>
                <h3 className={this.state.classesSaved ? "" : "hide"}>Updates have been saved.</h3>
                
                <form onSubmit={this.saveChanges}>
                    <div className="display-class">
                    {this.props.card.map(card => <ChallengeClass key={card.classId} card={card} captureChange={this.holdChanges} />)}
                    </div>
                    <button id="save_updates">Save</button>
                </form>
                <h5 className="white-font"><p>Bike/Tread: 20 minutes <br/>
                Strength/Yoga: 10 minutes <br/>
                Meditation/Stretch: 5 minutes <br/> 
                Always welcome to extend the listed target times!<br/>
                Don't forget to update and save as you check off those squares.</p></h5>
            </div>
        )
    }
}

const mapStateToProps = (state) =>
    ({  
        card: state.challenge,
        selectedParticipant : state.participant.selectedParticipant 
    });

const mapDispatchToProps = dispatch => bindActionCreators({
    updateCard
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ChallengeBody)
import logo from './logo.svg';
import './App.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ChallengeBody from './components/ChallengeBody'
import { Component } from 'react';
import { getParticipants } from './actions/participantActions'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import ParticipantList from './components/ParticipantList';
import Signup from './components/Signup';
import ReadyToPlay from './components/ReadyToPlay';
import TopNav from './components/TopNav';

class App extends Component {

  componentDidMount() {
    this.props.getParticipants();
  }

  render() {
    return (
      <div className="App">
        <Router>
          <TopNav />
          <Switch>
            <Route exact path="/">
              <ParticipantList/>
              <Signup/>
            </Route>
            <Route path="/challenge">
              <ChallengeBody />
            </Route>
            <Route path="/readytoplay">
              <ReadyToPlay />
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  getParticipants
}, dispatch)

export default connect(null, mapDispatchToProps)(App)

import {
    GET_PARTICIPANTS,
    FETCH_PARTICIPANTS_FAILED,
    NEW_PARTICIPANT,
    ADD_FAILED,
    SELECTED_PARTICIPANT,
    CLEAR_PARTICIPANT
} from '../actions/participantActions'

let initialState = {
    participants: [],
    selectedParticipant: ""
}

const participantReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PARTICIPANTS:
            console.log(action.payload);
            action.payload.sort((val1, val2) => (val1.leaderboardName.toLowerCase() < val2.leaderboardName.toLowerCase()) ? -1 : 1);
            console.log(action.payload)
            return { ...state, participants: action.payload }
        case SELECTED_PARTICIPANT:  
        case CLEAR_PARTICIPANT:
            return { ...state, selectedParticipant: action.payload }
        case NEW_PARTICIPANT:
            return { participants: [...state.participants, { leaderboardName: action.payload.leaderboardName, id: action.payload.id }], selectedParticipant: action.payload.leaderboardName }
        case FETCH_PARTICIPANTS_FAILED:
        case ADD_FAILED:
        default:
            return state
    }
}

export default participantReducer;